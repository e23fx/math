﻿using ParserEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//«Написать библиотечную функцию
//​ на вход принимает целочисленное значение в виде строки
//o​ строка может содержать любой символ;
//o​ строка может быть пустой;
//o​ строка может быть любой длины;
//o​ переданное число может быть как положительным, так и отрицательным;
//​ на выходе отдает целочисленное значение в виде строки, содержащее арифметически увеличенное на 1 входное значение
//​ Интерфейс метода "string Inc(string inputValue)";
//​ Регулярные выражения, сторонние библиотеки(кроме необходимых для тестирования) - не использовать
//​ Накрыть функциональность тестами(NUnit или MS Test)
//Ждем от Вас архив с проектом.

namespace MathPart.MathOperations
{
	public class IncCommand
	{
		public string Inc(string inputValue)
		{
			int number = 0;
			string sign = string.Empty;
			List<Lexem> lexems =
				ParseManager.Instance.GetLexems(inputValue);
			if(lexems.Count == 0)
			{
				return string.Empty;
			}
			Lexem lexemNumber = lexems.Last();
			if(lexemNumber.LexemType != LexemType.Number)
			{
				return string.Empty;
            }
			string lexemNumberValue = string.Empty;
			if (lexems.Count > 1)
			{
				for (int i = lexems.Count - 2; i >= 0; i--)
				{
					Lexem currentLexem = lexems.GetRange(i, 1)[0];
					if(currentLexem.LexemType == LexemType.Sign)
					{
						sign = currentLexem.LexemValue;
						break;
                    }
				}
				if(!string.IsNullOrEmpty(sign))
				{
					if(string.Compare(sign, "-") == 0)
					{
						lexemNumber.LexemValue = string.Format("-{0}", lexemNumber.LexemValue);
                    }
				}
			}
			int.TryParse(lexemNumber.LexemValue, out number);
			if (number == int.MaxValue)
			{
				throw new Exception("The number is very big");
			}
			else if (string.Compare(lexemNumber.LexemValue, "0") != 0 && number == 0)
			{
				throw new Exception("The number is very big");
			}
			number++;
			return number.ToString();
		}
    }
}
