﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ParserEngine
{
	public class ParseManager
	{
		#region Singletone
		[DebuggerStepThrough]
		private ParseManager()
		{
			init();
		}
		private static volatile ParseManager instance;
		private static object syncRoot = new Object();

		/// <summary>
		/// Singletone
		/// </summary>
		/// <returns></returns>
		public static ParseManager Instance
		{
			[DebuggerStepThrough]
			get
			{
				if (instance == null)
				{
					lock (syncRoot)
					{
						if (instance == null)
							instance = new ParseManager();
					}
				}
				return instance;
			}
		}
		#endregion

		List<Lexem> lexems;
		private List<Char> allowed1;
		private List<Char> allowed2;
		private List<Char> delimeters;
		private List<string> operators;

		private void init()
		{
			allowed1 = new List<char>();
			allowed1.Add('0');
			allowed1.Add('1');
			allowed1.Add('2');
			allowed1.Add('3');
			allowed1.Add('4');
			allowed1.Add('5');
			allowed1.Add('6');
			allowed1.Add('7');
			allowed1.Add('8');
			allowed1.Add('9');
			allowed2 = new List<char>();
			allowed2.Add('-');
			allowed2.Add('+');
			operators = new List<string>();
			operators.Add("-");
			operators.Add("+");
			delimeters = new List<char>();
			delimeters.Add(' ');
			delimeters.Add('\n');
			delimeters.Add('\t');
		}

		public List<Lexem> GetLexems(string inputValue)
		{
			int inputValueLength = inputValue.Length + 1;
			inputValue = string.Format("{0} ", inputValue);
			lexems = new List<Lexem>();
			StringBuilder currentLexem = new StringBuilder();
			LexemType currentLexemType = LexemType.None;
			LexemType currentLexemTypePrev = LexemType.None;
			for (int i = 0; i < inputValueLength; i++)
			{
                char ch = inputValue[i];
				// Check is operator
				if (currentLexemType == LexemType.Sign
					&& currentLexem.Length != 0
					&& operators.Contains(currentLexem.ToString()))
				{
					commitLexem(ref currentLexem, currentLexemType);
					currentLexemTypePrev = LexemType.None;
				}
				currentLexemType = LexemType.None;
				// Set lexem type
				if (allowed1.Contains(ch)) // is number char
				{
					currentLexemType = LexemType.Number;
                }
				else if (allowed2.Contains(ch)) // is operator char
				{
					currentLexemType = LexemType.Sign;
				}
				else if (delimeters.Contains(ch)) // is delimeter
				{
					currentLexemType = LexemType.Delimeter;
				}
				else // is other char
				{
					currentLexemType = LexemType.Symbol;
				}
				// Check conditions
				if(currentLexemType != currentLexemTypePrev) // turn
				{
					commitLexem(ref currentLexem, currentLexemTypePrev);
					if(currentLexemTypePrev == LexemType.Number)
					{
						break;
					}
				}
				if (currentLexemType != LexemType.Delimeter)
				{
					currentLexem.Append(ch);
				}
				currentLexemTypePrev = currentLexemType;
            }
			return lexems;
        }
		public void commitLexem(ref StringBuilder currentLexem, LexemType lexemType)
		{
			if(currentLexem.Length == 0)
			{
				return;
			}
			this.lexems.Add(new Lexem(lexemType, currentLexem.ToString()));
			currentLexem = new StringBuilder();
        }
	}
	public class Lexem
	{
		public Lexem(LexemType lexemType, string lexemValue)
		{
			this.LexemType = lexemType;
			this.LexemValue = lexemValue;
		}
		private LexemType lexemType;
		public LexemType LexemType
		{
			get
			{
				return this.lexemType;
			}
			set
			{
				this.lexemType = value;
			}
		}
		private string lexemValue;
		public string LexemValue
		{
			get
			{
				return this.lexemValue;
			}
			set
			{
				this.lexemValue = value;
			}
		}
	}
	public enum LexemType
	{
		None = 0,
		Delimeter = 1,
		Number = 2,
		Sign = 3,
		Symbol = 4,
	}
}