﻿using System;
using NUnit.Framework;
using MathPart.MathOperations;

namespace VendMachineTests
{
	[TestFixture]
	public class NUnitTest1
	{
		[Test]
		public void TestMethod1()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("");
			Assert.AreEqual(ret, "");
		}

		[Test]
		public void TestMethod2()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("0");
			Assert.AreEqual(ret, "1");
		}

		[Test]
		public void TestMethod3()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("1");
			Assert.AreEqual(ret, "2");
		}

		[Test]
		public void TestMethod4()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("-1");
			Assert.AreEqual(ret, "0");
		}

		[Test]
		public void TestMethod5()
		{
			IncCommand command = new IncCommand();
			try
			{
				string ret = command.Inc(string.Format("{0}", int.MaxValue));
			}
			catch
			{
				Assert.AreEqual(true, true);
				return;
			}
			Assert.AreEqual(true, false);
		}

		[Test]
		public void TestMethod6()
		{
			IncCommand command = new IncCommand();
			string value = new string('1', 100000000);
			try
			{
				string ret = command.Inc(value);
			}
			catch
			{
				Assert.AreEqual(true, true);
				return;
			}
			Assert.AreEqual(true, false);
		}

		[Test]
		public void TestMethod7()
		{
			IncCommand command = new IncCommand();
			string value = new string('1', 100);
			try
			{
				string ret = command.Inc(value);
			}
			catch
			{
				Assert.AreEqual(true, true);
				return;
			}
			Assert.AreEqual(true, false);
		}

		[Test]
		public void TestMethod8()
		{
			IncCommand command = new IncCommand();
			string value = new string('1', 6);
			string ret = command.Inc(value);
			Assert.AreEqual(ret, "111112");
		}

		[Test]
		public void TestMethod9()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc(string.Format("{0}", int.MinValue));
			Assert.AreEqual(ret, "-2147483647");
		}

		[Test]
		public void TestMethod10()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc(string.Format("t {0}", int.MinValue));
			Assert.AreEqual(ret, "-2147483647");
		}

		[Test]
		public void TestMethod11()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("+ 6");
			Assert.AreEqual(ret, "7");
		}

		[Test]
		public void TestMethod12()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("+8");
			Assert.AreEqual(ret, "9");
		}

		[Test]
		public void TestMethod13()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("d\n --oih6i9k-- + 3y");
			Assert.AreEqual(ret, "-5");
		}

		[Test]
		public void TestMethod14()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("aa10");
			Assert.AreEqual(ret, "11");
		}

		[Test]
		public void TestMethod15()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("a☺a-☻10");
			Assert.AreEqual(ret, "-9");
		}

		[Test]
		public void TestMethod16()
		{
			IncCommand command = new IncCommand();
			string ret = command.Inc("-,10,");
			Assert.AreEqual(ret, "-9");
		}
	}
}